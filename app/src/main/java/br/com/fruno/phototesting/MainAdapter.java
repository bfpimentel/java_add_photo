package br.com.fruno.phototesting;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.RecyclerView.Adapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MainAdapter extends Adapter<MainAdapter.MainViewHolder> {

    private List<Photo> list;
    private MainViewHolder holder;

    MainAdapter(List<Photo> list) {
        this.list = list;
    }

    @NonNull
    @Override
    public MainViewHolder onCreateViewHolder(@NonNull ViewGroup itemView, int position) {
        View view = LayoutInflater.from(itemView.getContext())
                .inflate(R.layout.activity_main_item, itemView, false);
        holder = new MainViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull MainViewHolder mainViewHolder, int position) {
        holder.bind(list.get(position));
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    class MainViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.item_iv)
        ImageView ivPhoto;
        @BindView(R.id.item_tv_name)
        TextView tvName;
        @BindView(R.id.item_tv_size)
        TextView tvSize;

        MainViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        void bind(Photo photo) {
            Glide.with(itemView).load(photo.getPath()).into(ivPhoto);
            tvName.setText(photo.getName());
            tvSize.setText(photo.getSize());
        }
    }
}
